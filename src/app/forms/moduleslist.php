<?php
namespace app\forms;

use std, gui, framework, app;

class moduleslist extends AbstractForm {

    /**
     * @event showing 
     */
    function doShowing(UXWindowEvent $e = null) {    
        $this->getmoduleslist();
    }

    /**
     * @event applymoduleslist.action 
     */
    function doApplymoduleslistAction(UXEvent $e = null) {
        Animation::fadeOut($this , 1000);
        Animation::fadeOut($this , 1000 , function () {
            $Settings = app()->getForm(Settings);
            $Settings->list->items->clear();
            $Settings->list->items->add($this->moduleiteam->selected);
            $this->bdini->set('key' , $this->moduleiteam->selected , $Settings->bd->selected);
            $this->free();
        });
    }

    /**
     * @event exitmoduleslist.action 
     */
    function doExitmoduleslistAction(UXEvent $e = null) {
        Animation::fadeOut($this , 1000);
        Animation::fadeOut($this , 1000 , function () {
            $this->free();
        });
    }
}
