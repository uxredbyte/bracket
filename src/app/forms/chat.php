<?php
namespace app\forms;

use std, gui, framework, app;

class chat extends AbstractForm {

    /**
     * @event hide 
     */
    function doHide(UXWindowEvent $e = null) {    
        $this->Menu(false);
    }

    /**
     * @event show 
     */
    function doShow(UXWindowEvent $e = null) {    
        $this->centerOnScreen();
    }

    /**
     * @event settings.action 
     */
    function doSettingsAction(UXEvent $e = null) {    
        $SettingsChat = app()->getForm(SettingsChat);
        $SettingsChat->opacity = 0;
        Animation::fadeIn($SettingsChat , 1000);
        $SettingsChat->showAndWait();
    }

    /**
     * @event text.keyDown-Enter 
     */
    function doTextKeyDownEnter(UXKeyEvent $e = null) {
        $this->SendChat(0 , $e->sender->text , false);
        $this->clearfirstiteamprofile();
        $e->sender->clear();
    }

    /**
     * @event clearchat.action 
     */
    function doClearchatAction(UXEvent $e = null) {    
        $this->textArea->clear();
        $this->toast('Успешно очистился чат!');
    }
}
