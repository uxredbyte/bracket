<?php
namespace app\forms;

use app\classes\jTelegramApi;
use std, gui, framework, app;
use app\modules\vkModule as VK;

class Settings extends AbstractForm {


    /**
     * @event hide 
     */
    function doHide(UXWindowEvent $e = null) {
        $this->Menu(false);
        $this->Save();
        //widget
        $MainForm = app()->getForm(MainForm);
        $screen = UXScreen::getPrimary();
        $ScreenWidth = $screen->bounds['width'] / 2;//ширина
        $ScreenHeight = $screen->bounds['height'] / 2;//высота
        $MainForm->x = $ScreenWidth + $this->valueX->value;
        $MainForm->y = $ScreenHeight + $this->valueY->value;
    }

    /**
     * @event colorcorrection.click-Left 
     */
    function doColorcorrectionClickLeft(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        if($e->sender->selected) {
            $MainForm->image->colorAdjustEffect->enable();
            $MainForm->image->colorAdjustEffect->brightness = $this->brightness->value;
            $MainForm->image->colorAdjustEffect->contrast = $this->contrast->value;
            $MainForm->image->colorAdjustEffect->hue = $this->hue->value;
            $MainForm->image->colorAdjustEffect->saturation = $this->saturation->value;
            $MainForm->toast('Цветовая коррекция->Вкл');
        }
        else {
            $MainForm->image->colorAdjustEffect->disable();
            $MainForm->toast('Цветовая коррекция->Выкл');
        }
    }

    /**
     * @event brightness.mouseDrag 
     */
    function doBrightnessMouseDrag(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->colorAdjustEffect->brightness = $e->sender->value;
    }

    /**
     * @event brightness.mouseDown 
     */
    function doBrightnessMouseDown(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->colorAdjustEffect->brightness = $e->sender->value;
    }

    /**
     * @event contrast.mouseDrag 
     */
    function doContrastMouseDrag(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->colorAdjustEffect->contrast = $e->sender->value;
    }

    /**
     * @event contrast.mouseDown 
     */
    function doContrastMouseDown(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->colorAdjustEffect->contrast = $e->sender->value;
    }

    /**
     * @event hue.mouseDown 
     */
    function doHueMouseDown(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->colorAdjustEffect->hue = $e->sender->value;
    }

    /**
     * @event hue.mouseDrag 
     */
    function doHueMouseDrag(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->colorAdjustEffect->hue = $e->sender->value;
    }

    /**
     * @event saturation.mouseDown 
     */
    function doSaturationMouseDown(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->colorAdjustEffect->saturation = $e->sender->value;
    }

    /**
     * @event saturation.mouseDrag 
     */
    function doSaturationMouseDrag(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->colorAdjustEffect->saturation = $e->sender->value;
    }

    /**
     * @event reloadcolorcorrection.action 
     */
    function doReloadcolorcorrectionAction(UXEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $this->brightness->value = 0;
        $this->contrast->value = 0;
        $this->hue->value = 0;
        $this->saturation->value = 0;
        $this->Save();
        $MainForm->toast('Эффект был восстановлен');
        $this->LoadLeaf($MainForm->image);
    }

    /**
     * @event selectedShadow.click-Left 
     */
    function doSelectedShadowClickLeft(UXMouseEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        if($e->sender->selected) {
            $MainForm->image->dropShadowEffect->enable();
            $MainForm->image->dropShadowEffect->radius = $this->radiusShadow->value;
            $MainForm->image->dropShadowEffect->spread = $this->Intensity->value;
            $MainForm->image->dropShadowEffect->offsetX = $this->transetX->value;
            $MainForm->image->dropShadowEffect->offsetY = $this->transetY->value;
            $MainForm->toast('Отбрасываемая тень->Вкл');
        }
        else {
            $MainForm->image->dropShadowEffect->disable();
            $MainForm->toast('Отбрасываемая тень->Выкл');
        }
    }

    /**
     * @event resetShadow.action 
     */
    function doResetShadowAction(UXEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        $this->colorPicker->value = UXColor::of('#b3b3b3');
        $this->radiusShadow->value = 10;
        $this->Intensity->value = 0;
        $this->transetX->value = 0;
        $this->transetY->value = 0;
        $this->Save();
        $MainForm->toast('Эффект был восстановлен');
        $this->LoadLeaf($MainForm->image);
    }

    /**
     * @event transetY.mouseDown 
     */
    function doTransetYMouseDown(UXMouseEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->dropShadowEffect->offsetY = $e->sender->value;
    }

    /**
     * @event transetY.mouseDrag 
     */
    function doTransetYMouseDrag(UXMouseEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->dropShadowEffect->offsetY = $e->sender->value;
    }

    /**
     * @event transetX.mouseDown 
     */
    function doTransetXMouseDown(UXMouseEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->dropShadowEffect->offsetX = $e->sender->value;
    }

    /**
     * @event transetX.mouseDrag 
     */
    function doTransetXMouseDrag(UXMouseEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->dropShadowEffect->offsetX = $e->sender->value;
    }

    /**
     * @event Intensity.mouseDrag 
     */
    function doIntensityMouseDrag(UXMouseEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->dropShadowEffect->spread = $e->sender->value;
    }

    /**
     * @event Intensity.mouseDown 
     */
    function doIntensityMouseDown(UXMouseEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->dropShadowEffect->spread = $e->sender->value;
    }

    /**
     * @event radiusShadow.mouseDrag 
     */
    function doRadiusShadowMouseDrag(UXMouseEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->dropShadowEffect->radius = $e->sender->value;
    }

    /**
     * @event radiusShadow.mouseDown 
     */
    function doRadiusShadowMouseDown(UXMouseEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->dropShadowEffect->radius = $e->sender->value;
    }

    /**
     * @event spoilerAlt.click-Left 
     */
    function doSpoilerAltClickLeft(UXMouseEvent $e = null) {    
        if($e->sender->expanded == true) {
            $e->sender->toFront();
        }
        else {
            //$e->sender->toBack();
        }
    }

    /**
     * @event spoiler.click-Left 
     */
    function doSpoilerClickLeft(UXMouseEvent $e = null) {    
        if($e->sender->expanded == true) {
            $e->sender->toFront();
        }
        else {
            $e->sender->toBack();
        }
    }

    /**
     * @event colorPicker.action 
     */
    function doColorPickerAction(UXEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->dropShadowEffect->color = $e->sender->value;
    }

    /**
     * @event Apply.action 
     */
    function doApplyAction(UXEvent $e = null) {
        $MainForm = app()->getForm(MainForm);
        $screen = UXScreen::getPrimary();
        $ScreenWidth = $screen->bounds['width'] / 2;//ширина
        $ScreenHeight = $screen->bounds['height'] / 2;//высота
        $MainForm->x = $ScreenWidth + $this->valueX->value;
        $MainForm->y = $ScreenHeight + $this->valueY->value;
    }

    /**
     * @event addbd.keyDown-Enter 
     */
    function doAddbdKeyDownEnter(UXKeyEvent $e = null) {    
        $this->addbd();
    }

    /**
     * @event bd.action 
     */
    function doBdAction(UXEvent $e = null) {    
        $this->selectedbd();
    }
    
    function selectedbd () {
        $selected = $this->bd->selected;
        $this->magicmodules->selected = $this->bdini->get('magicmodules' , $selected);
        $magicmodule = $this->magicmodules->selected;
        if ($magicmodule == true) {
            $this->text->enabled = false;
        } else {
            $this->text->enabled = true;
        }
        $this->list->itemsText = $this->bdini->get('key' , $selected);
        $this->allprice->selected = $this->bdini->get('all' , $selected);
        $this->checkbox_img->selected = $this->bdini->get('img' , $selected);
        $this->addbd->text = $selected;
        $this->checkmodule(false);
    }

    function checkmodule ($checkmode) {
        if ($this->magicmodules->selected && $checkmode == true && str::startsWith($this->bd->selected , '/')) {
            if(uiConfirm('Внимание вы точно хотите перейти в модульный режим ?')) {
                $this->toast('Успешно перешли в новый режим');
                $this->magicmodules->selected = true;
                $this->list->items->clear();
                $this->bdini->set('key' , $this->list->itemsText , $this->bd->selected);
                app()->getForm(moduleslist)->showAndWait();
            } else {
                $this->toast('Успешно не перешли в новый режим');
                $this->magicmodules->selected = false;
            }
            $this->bdini->set('magicmodules' , $this->magicmodules->selected , $this->bd->selected);
            $this->text->enabled = !$this->magicmodules->selected;
            $this->add->enabled = !$this->magicmodules->selected;
            $this->deleteX->enabled = !$this->magicmodules->selected;
        } else if (!$this->magicmodules->selected && $checkmode == true) {
            if(uiConfirm('Внимание вы точно хотите уйти от этого режима ?')) {
                $this->toast('Успешно ушли от этого режима');
                $this->magicmodules->selected = false;
                $this->list->items->clear();
                $this->bdini->set('key' , $this->list->itemsText , $this->bd->selected);
            } else {
                $this->toast('Успешно перешли в новый режим');
                $this->magicmodules->selected = true;
            }
            $this->bdini->set('magicmodules' , $this->magicmodules->selected , $this->bd->selected);
            $this->text->enabled = !$this->magicmodules->selected;
            $this->add->enabled = !$this->magicmodules->selected;
            $this->deleteX->enabled = !$this->magicmodules->selected;
        } else if ($checkmode == true) {
            UXDialog::showAndWait('Ошибка отсуствует символ / , он должен быть первым' , 'ERROR');
            $this->magicmodules->selected = false;
            $this->bdini->set('key' , $this->list->itemsText , $this->bd->selected);
        }
        $this->selectedmoduleindex->enabled = $this->magicmodules->selected;
    }
    
    /**
     * @event text.keyDown-Enter 
     */
    function doTextKeyDownEnter(UXKeyEvent $e = null) {    
        $this->additeambd();
    }

    /**
     * @event list.action 
     */
    function doListAction(UXEvent $e = null) {    
        $this->text->text = $e->sender->selectedItem;
    }

    /**
     * @event list.keyDown-Delete 
     */
    function doListKeyDownDelete(UXKeyEvent $e = null) {    
        $this->deleteElementIteambd();
    }
    
    function deleteElementIteambd() {
        if($this->list->selectedItem != null && !$this->magicmodules->selected) {
            $this->toast('Уничтожена->' . $this->list->selectedItem);
            $this->bdini->removeSection($this->list->selectedItem);
            $this->list->items->removeByIndex($this->list->selectedIndex);
        }
        else {
            if ($this->magicmodules->selected) {
                $this->toast('Невозможно удалить в режиме модуля');
            } else {
                $this->toast('Невозможно удалить ничего = )');
            }
        }
    }


    /**
     * @event addbaza.action 
     */
    function doAddbazaAction(UXEvent $e = null) {
        $this->addbd();
    }
    
    function addbd() {
        if (!str::startsWith($this->addbd->text , '/')) {
            if($this->addbd->text != null) {
                foreach ($this->bd->items as $val) {
                    if($val == $this->addbd->text) {
                        $this->toast('Такое уже есть лол');
                        $this->addbd->clear();
                        return;
                    }
                }
                $this->bd->items->add($this->addbd->text);
                $this->bdini->set('key' , $this->bd->itemsText , 'section');
                $this->bdini->set('key' , '' , $this->addbd->text);
                $this->bd->selected = $this->addbd->text;
                $this->addbd->clear();
            }
            else {
                $this->toast('Нужно ввести текст');
            }
        } else {
            $text = str::lower($this->addbd->text);
            $this->bd->items->add($text);
            $this->bdini->set('key' , $this->bd->itemsText , 'section');
            $this->bdini->set('key' , '' , $text);
            $this->bd->selected = $text;
            $this->addbd->clear();
        }
    }

    /**
     * @event Deletebd.action 
     */
    function doDeletebdAction(UXEvent $e = null) {
        $this->deletebd();
    }
    
    function deletebd() {
        $this->bdini->removeSection($this->bd->selected);
        $this->toast('Уничтожена->' . $this->bd->selected);
        $this->bd->items->removeByIndex($this->bd->selectedIndex);
        $this->bdini->set('key' , $this->bd->itemsText , 'section');
        if($this->bd->selected == null) {
            $this->bd->selectedIndex = 0;
        }
    }


    /**
     * @event Category_skin.action 
     */
    function doCategory_skinAction(UXEvent $e = null) {    
        $this->skin->items->clear();
        $files = fs::scan('skin/' . $e->sender->selected . '/', ['extensions' => ['jpg', 'png' , 'jpeg'], 'excludeDirs' => true]);
        foreach ($files as $fs) {
            $path = str::split($fs , "\\");
            $this->skin->items->add($path[2]);
        }
        if($this->skin->selected == null) {
            $this->skin->selectedIndex = 0;
        }
    }

    /**
     * @event skin.action 
     */
    function doSkinAction(UXEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->image->image = new UXImage('skin/' . $this->Category_skin->selected . '/' . $e->sender->selected);
    }

    /**
     * @event Category_skin.construct 
     */
    function doCategory_skinConstruct(UXEvent $e = null) { 
        $this->reloadskin();
    }

    /**
     * @event sizeimage.mouseDrag 
     */
    function doSizeimageMouseDrag(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->width = $e->sender->value;
        $MainForm->image->width = $MainForm->width;
        $MainForm->height = $e->sender->value;
        $MainForm->image->height = $MainForm->height;
    }

    /**
     * @event sizeimage.click 
     */
    function doSizeimageClick(UXMouseEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        $MainForm->width = $e->sender->value;
        $MainForm->image->width = $MainForm->width;
        $MainForm->height = $e->sender->value;
        $MainForm->image->height = $MainForm->height;  
    }

    /**
     * @event loginvk.action 
     */
    function doLoginvkAction(UXEvent $e = null) {    
        VK::checkAuth();
    }

    /**
     * @event longpoll.action 
     */
    function doLongpollAction(UXEvent $e = null) {    
        $MainForm = app()->getForm(MainForm);
        if($e->sender->selected) {
            $e->sender->text= 'Отключить long-Poll';
            $e->sender->graphic = new UXImageView (new UXImage('res://.data/img/Exit.png'));
            VK::longPollConnect(function($updates) use ($MainForm) {
                foreach($updates as $update) {
                    switch($update[0]) {
                        case '4':
                            $Settings = app()->getForm(Settings);
                            if($Settings->idgroup->selected == true) {
                                if($updates[7]['from'] != 479197008) {
                                    $module4 = new module4();
                                    $module4->module4_get($update[3] , $update[6]);
                                }
                            }
                            else {
                                if($update[2] != 35) {
                                   $module4 = new module4();
                                   $module4->module4_get($update[3] , $update[6]);
                                }
                            }
                        break;
                        
                        case '8':
                            //$this->lpText->text = ('Пользователь id' . $update[1] . ' стал онлайн');
                        break;
                        
                        case '9':
                            //$this->lpText->text = ('Пользователь id' . $update[1] . ' стал оффлайн');
                        break;
                       
                        case '61':
                            //$this->lpText->text = ('Пользователь id' . $update[1] . ' набирает сообщение');
                        break;
                    }
                }
            });
            $MainForm->toast('long-poll подключился успешно!');
        } 
        else {
            $e->sender->text = 'Подключиться к long-poll';
            VK::longPollDisconnect();
            $e->sender->graphic = new UXImageView (new UXImage('res://.data/img/action.png'));
            $MainForm->toast('long-poll отключился успешно!');
        }
    }


    /**
     * @event keyDown-Ctrl+S 
     */
    function doKeyDownCtrlS(UXKeyEvent $e = null) {    
        $this->savebd();
        $this->toast('Успешно сохранилась база данных!');
    }

    function smoothAnimation (UXToggleButton $e = null , UXPanel $panel = null , $time) {
        if ($e->selected) {
            $e->enabled = false;
            $panel->visible = true;
            Animation::fadeIn($panel , $time);
            Animation::fadeIn($panel , $time , function () use ($e) {
                $e->enabled = true;
            });
        } else {
            $e->enabled = false;
            Animation::fadeOut($panel , $time);
            Animation::fadeOut($panel , $time , function () use ($e , $panel) {
                $e->enabled = true;
                $panel->visible = false;
            });
        }
    }
    
    /**
     * @event Asynx_token.action 
     */
    function doAsynx_tokenAction(UXEvent $e = null) {    
        $token = $this->token->text;
        $proxy = $this->proxyTelegram->text;
        if ($e->sender->selected) {
            $jTelegramApi = new jTelegramApi();
            $jTelegramApi->setToken($token);
            $jTelegramApi->setProxy($proxy);
            if ($jTelegramApi->LongPoll(2) == true) {
                Logger::info('Успешная авторизация!');
                $this->timer->start();
            } else {
                Logger::info('Внимание токен неверный!');
                $this->Asynx_token->selected = false;
            }
        } else {
            Logger::info("Deactivation Telegram_api => OK");
            $this->timer->stop();
        }
    }

    /**
     * @event editsection.action 
     */
    function doEditsectionAction(UXEvent $e = null)
    {
        if ($this->bd->selected && trim($this->addbd->text) != null) {
            $this->toast($this->bd->selected . " => " . trim($this->addbd->text));
            $this->bdini->set('key' , trim($this->addbd->text) , $this->bd->selected);
        } else {
            $this->toast("");
        }
    }

    /**
     * @event createmodule.action 
     */
    function doCreatemoduleAction(UXEvent $e = null) {    
        
    }

    /**
     * @event openfile.action 
     */
    function doOpenfileAction(UXEvent $e = null) {
        $this->fileChooserimg->execute();
    }
    
    function replaceiteambd () {
        if ($this->list->selectedItem != null && !$this->magicmodules->selected) {
            foreach ($this->list->items as $value) {
                if ($this->text->text == $value) {
                    $this->toast('Такое уже есть лол');
                    $this->text->clear();
                    return;
                }
            }
            $this->toast('Ответ->' . $this->list->selectedItem . ' | заменен на ->' . $this->text->text);
            $this->list->items->replace($this->list->selectedItem , $this->text->text);
        }
        else {
            if ($this->magicmodules->selected) {
                $this->toast('Невозможно заменить в режиме модуля');
            } else {
                $this->toast('Невозможно заменить ничего = )');
            }
        }
    }
    
    /**
     * @event replacelist.action 
     */
    function doReplacelistAction(UXEvent $e = null) {
        $this->replaceiteambd();
    }

    /**
     * @event deleteX.action 
     */
    function doDeleteXAction(UXEvent $e = null) {
        $this->deleteElementIteambd();
    }

    function additeambd () {
        if($this->text->text != null) {
            if($this->list->items->isEmpty() == true) {
                $this->list->items->add($this->text->text);
                $this->toast('Дабавлен ответ->' . $this->text->text . ' | на этот вопрос->' . $this->bd->selected);
                $this->bdini->set('key' , $this->list->itemsText , $this->bd->selected);
                $this->text->clear();
            }
            else {
                foreach ($this->list->items as $item) {
                    if($item == $this->text->text) {
                        $this->toast('Такое уже есть лол');
                        $this->text->clear();
                        return;
                    }
                }
                $this->list->items->add($this->text->text);
                $this->toast('Дабавлен ответ->' . $this->text->text . ' | на этот вопрос->' . $this->bd->selected);
                $this->bdini->set('key' , $this->list->itemsText , $this->bd->selected);
                $this->text->clear();
            }
        }
        else {
            $this->toast('Такое уже есть лол');
            $this->text->clear();
        }
    }

    /**
     * @event add.action 
     */
    function doAddAction(UXEvent $e = null) {
        $this->additeambd();
    }


    /**
     * @event selectedmoduleindex.action 
     */
    function doSelectedmoduleindexAction(UXEvent $e = null) {    
        app()->getForm(moduleslist)->showAndWait();
    }

    /**
     * @event magicmodules.click-Left 
     */
    function doMagicmodulesClickLeft(UXMouseEvent $e = null) {    
        $this->checkmodule(true);
    }

    /**
     * @event selectedmoduleindex.construct 
     */
    function doSelectedmoduleindexConstruct(UXEvent $e = null) {    
        $this->checkmodule(false);
    }

    /**
     * @event bd.keyDown-Delete 
     */
    function doBdKeyDownDelete(UXKeyEvent $e = null) {    
        $this->deletebd();
    }


    /**
     * @event effect.action 
     */
    function doEffectAction(UXEvent $e = null) {
        if($e->sender->selected) {
            $this->Female->selected = !$e->sender->selected;
            $this->Male->selected = false;
            $this->toggleButtonmodulesystem->selected = false;
            //toggle
            $this->leafaddons->visible = true;
            //off
            $this->Malepanel->visible = false;
            $this->Femalepanel->visible = false;
            $this->modulesystem->visible = false;
        }
        else {
            if($e->sender->selected == false) {
                if($this->Female->selected == false) {
                    $this->Female->selected = true;
                }
            }
            //on
            $this->Femalepanel->visible = true;
            //off
            $this->leafaddons->visible = false;
        }
    }

    /**
     * @event toggleButtonmodulesystem.action 
     */
    function doToggleButtonmodulesystemAction(UXEvent $e = null) {
        if($e->sender->selected) {
            $this->Female->selected = false;
            $this->Male->selected = false;
            $this->effect->selected = false;
            //toggle
            $this->modulesystem->visible = true;
            //off
            $this->Malepanel->visible = false;
            $this->Femalepanel->visible = false;
            $this->leafaddons->visible = false;
        }
        else {
            //on
            $this->Femalepanel->visible = true;
            //off
            $this->leafaddons->visible = false;
        }
    }

    /**
     * @event Female.action 
     */
    function doFemaleAction(UXEvent $e = null) {
        if($e->sender->selected) {
            $this->Male->selected = !$e->sender->selected;
            $this->effect->selected = false;
            $this->toggleButtonmodulesystem->selected = false;
            //toggle
            $this->Femalepanel->visible = true;
            //off
            $this->Malepanel->visible = false;
            $this->leafaddons->visible = false;
            $this->modulesystem->visible = false;
        }
        else {
            if($e->sender->selected == false) {
                if($this->Male->selected == false) {
                    $this->Male->selected = true;
                }
            }
            //on
            $this->Malepanel->visible = true;
            //off
            $this->Femalepanel->visible = false;
        }
    }

    /**
     * @event Male.action 
     */
    function doMaleAction(UXEvent $e = null) {
        if($e->sender->selected) {
            $this->Female->selected = !$e->sender->selected;
            $this->effect->selected = false;
            $this->toggleButtonmodulesystem->selected = false;
            //toggle
            $this->Malepanel->visible = true;
            //off
            $this->Femalepanel->visible = false;
            $this->leafaddons->visible = false;
            $this->modulesystem->visible = false;
        }
        else {
            if($e->sender->selected == false) {
                if($this->Female->selected == false) {
                    $this->Female->selected = true;
                }
            }
            //on
            $this->Malepanel->visible = false;
            //off
            $this->Femalepanel->visible = true;
        }
    }

    /**
     * @event info.action 
     */
    function doInfoAction(UXEvent $e = null) {
        $this->toast('Автор:->Меркус');
    }

    /**
     * @event mod.action 
     */
    function doModAction(UXEvent $e = null) {
        if ($e->sender->selected) {
            $this->telegram->enabled = false;
            $this->vk->enabled = false;
            $this->widgetgirl->enabled = false;
        } else {
            $this->telegram->enabled = true;
            $this->vk->enabled = true;
            $this->widgetgirl->enabled = true;
        }
        $this->smoothAnimation($e->sender , $this->skinpanel , 1000);
    }

    /**
     * @event telegram.action 
     */
    function doTelegramAction(UXEvent $e = null) {
        if ($e->sender->selected) {
            $this->mod->enabled = false;
            $this->vk->enabled = false;
            $this->widgetgirl->enabled = false;
        } else {
            $this->mod->enabled = true;
            $this->vk->enabled = true;
            $this->widgetgirl->enabled = true;
        }
        $this->smoothAnimation($e->sender , $this->paneltelegram , 1000);
    }

    /**
     * @event vk.action 
     */
    function doVkAction(UXEvent $e = null) {
        if ($e->sender->selected) {
            $this->telegram->enabled = false;
            $this->mod->enabled = false;
            $this->widgetgirl->enabled = false;
        } else {
            $this->telegram->enabled = true;
            $this->mod->enabled = true;
            $this->widgetgirl->enabled = true;
        }
        $this->smoothAnimation($e->sender , $this->panelvk , 1000);
    }

    /**
     * @event widgetgirl.action 
     */
    function doWidgetgirlAction(UXEvent $e = null) {
        if ($e->sender->selected) {
            $this->telegram->enabled = false;
            $this->vk->enabled = false;
            $this->mod->enabled = false;
        } else {
            $this->telegram->enabled = true;
            $this->vk->enabled = true;
            $this->mod->enabled = true;
        }  
        $this->smoothAnimation($e->sender , $this->panelwidget , 1000);
    }

    /**
     * @event hide.action 
     */
    function doHideAction(UXEvent $e = null) {
        Animation::fadeOut($this , 1000);
        Animation::fadeOut($this , 1000 , function() {
            $this->hide();
        });
    }
}