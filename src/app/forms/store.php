<?php
namespace app\forms;

use std, gui, framework, app;

class store extends AbstractForm {

    /**
     * @event hide 
     */
    function doHide(UXWindowEvent $e = null) {    
        $this->Menu(false);//Вызов контекстного меню запрещено!
        $this->reloadskin();//Загрузка скина
        $this->free();//Самоуничтожение формы
    }

    /**
     * @event show 
     */
    function doShow(UXWindowEvent $e = null) {    
        $this->getSkins();//Получение скина
    }

    /**
     * @event installskin.action 
     */
    function doInstallskinAction(UXEvent $e = null) {    
        $this->downloadskin();//Устновка скина
    }

    /**
     * @event pagination.action 
     */
    function doPaginationAction(UXEvent $e = null) {
        $this->setSkin($this->image);//Установка скина 
    }
}
