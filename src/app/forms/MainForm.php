<?php
namespace app\forms;

use std, gui, framework, app;

class MainForm extends AbstractForm {

    /**
     * @event image.click-Right 
     */
    function doImageClickRight(UXMouseEvent $e = null) {
        //-------------------------------------------------------------------------------
            $this->ContextMenuShow($e->sender , $e->x , $e->y);//Вызвать функцию контекстного меню
        //-------------------------------------------------------------------------------
    }

    /**
     * @event show 
     */
    function doShow(UXWindowEvent $e = null) {    
        $this->Load();//Загрузки ini + проверка на обновление
    }

    /**
     * @event image.construct 
     */
    function doImageConstruct(UXEvent $e = null) {
        $this->LoadLeaf($e->sender);//Загрузка эффектов
    }
}