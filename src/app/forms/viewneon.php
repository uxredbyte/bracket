<?php
namespace app\forms;

use std, gui, framework, app;


class viewneon extends AbstractForm
{

    /**
     * @event browser.load 
     */
    function doBrowserLoad(UXEvent $e = null)
    {    
        $Settings = app()->getForm(Settings);
        $TelegramAPI = new Telegram_api();
        $TelegramAPI->setToken($Settings->token->text);
        $TelegramAPI->sendPhoto_id($Settings->chatid->text , $e->sender->snapshot());
        $this->hide();
    }
}
