<?php
namespace app\classes;

use facade\Json;
use std, gui, framework, app;

class jTelegramApi  {

    /**
     * Установить токен
     * @return string
     */
    public function setToken ($token) {
        $GLOBALS['token_telegramm'] = "https://api.telegram.org/bot$token/";
    }
    
    /**
     * Получить токен
     * @return string
     */
    public function getToken () {
        return $GLOBALS['token_telegramm'];
    }
    
    /**
     * Установка прокси
     * @return string
     */
    public function setProxy ($proxy) {
        $GLOBALS['proxy_telegramm'] = $proxy;
    }
    
    /**
     * Возвращение прокси
     * @return string
     */
    public function getProxy () {
        return $GLOBALS['proxy_telegramm'];
    }
    
    /**
     * Задать прошлый id сообщение
     * @return string
     */
    public function setlastmessage ($string) {
        $GLOBALS['lastmessage'] = $string;
    }
    
    /**
     * Возвратить прошлый id сообщение
     * @return string
     */
    public function getlastmessage () {
        return $GLOBALS['lastmessage'];
    }

    /**
     * //
     * => Получение обновление LongPoll и обработать
     * //
     * [Дополнительные]----------------------------------------
     * => [0] : Возвращаем последнее сообщение (Проверка id)
     * => [2] : Возвращаем проверку токена
     * [message]-------------------------------------
     * => [1] : Возвращаем последний message_id
     * => [13] : Возвращаем последний text (Без проверки на id)
     * [from]----------------------------------------
     * => [3] : Вернуть id
     * => [4] : Вернуть is_bot
     * => [5] : Вернуть first_name
     * => [6] : Вернуть last_name
     * => [7] : Вернуть username
     * => [8] : Вернуть language_code
     * [chat]----------------------------------------
     * => [9] : Вернуть chat_id
     * => [10] : Вернуть title
     * => [11] : Вернуть username
     * => [12] : Вернуть type
     * @return string
     */
    public function LongPoll ($code) {
        $url_request = $this->getToken() . 'getUpdates?offset=-1';
        $request = curl_init($url_request);
        curl_setopt($request , 'CURLOPT_PROXY' , $this->getProxy());
        curl_exec_async($request , function ($data) use ($code) {
            $value = Json::decode($data);
            //message
                $message_id = $value['result'][0]['message']['message_id'];
            
            switch ($code) {
                case 0:
                    $chat_id = $value['result'][0]['message']['chat']['id']; //chat id
                    if ($this->getlastmessage() != $value['result'][0]['message']['message_id'] && $chat_id == app()->getForm(Settings)->chatid->text) {
                        $this->setlastmessage($value['result'][0]['message']['message_id']);
                        Logger::info('Новое сообщение => ' . $value['result'][0]['message']['text']);
                        $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['text'];
                    } else {
                        $GLOBALS['return_callback_telegramm'] = 1;
                        Logger::info('Ожидание нового сообщение...' . $GLOBALS['return_callback_telegramm']);
                    }
                break;
                case 1:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['message_id'];
                break;
                case 2:
                    if ($value['ok'] == 1) {
                        $GLOBALS['return_callback_telegramm'] = true;
                    } else {
                        $GLOBALS['return_callback_telegramm'] = false;
                    }
                break;
                case 3://from
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['from']['id']; //from id
                break;
                case 4:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['from']['is_bot']; //from is_bot
                break;
                case 5:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['from']['first_name']; //from first_name
                break;
                case 6:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['from']['last_name']; //from last_name
                break;    
                case 7:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['from']['username']; //from username
                break;
                case 8:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['from']['language_code']; //from lang
                break; //chat
                case 9:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['chat']['id']; //chat id
                break;
                case 10:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['chat']['title']; //chat title
                break;
                case 11:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['chat']['username']; //chat username
                break;
                case 12:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['chat']['type']; //chat type
                break;
                case 13:
                    $GLOBALS['return_callback_telegramm'] = $value['result'][0]['message']['text']; //text
                break;
            }
        });
        curl_close($request);
        return $GLOBALS['return_callback_telegramm'];
        
    }

    /**
     * Отправить текст
     * @return string
     */
    public function sendMessage_id ($chat_id , $text) {
        $url_request = $this->getToken() . "sendMessage?chat_id=$chat_id&text=$text";
        $request = curl_init($url_request);
        curl_setopt($request , 'CURLOPT_PROXY' , $this->getProxy());
        curl_exec_async($request , function () {});
        curl_close($request);
    }
    
    /**
     * Отправить фото
     * @return string
     */
    public function sendPhoto_id ($chat_id , $photo) {
        $url_request = $this->getToken() . "sendPhoto";
        $request = curl_init($url_request);
        $post = array(
            'chat_id' => $chat_id ,
            'photo' => '@' . $photo
        );
        curl_setopt($request , 'CURLOPT_POST' , 1);
        curl_setopt($request , 'CURLOPT_POSTFIELDS' , $post);
        curl_setopt($request , 'CURLOPT_RETURNTRANSFER' , 1);
        curl_setopt($request , 'CURLOPT_PROXY' , $this->getProxy());
        curl_exec_async($request , function () {});
        curl_close ($request);
    }
    
    /**
     * Отправить Документ
     * @return string
     */
    public function sendDocument_id ($chat_id , $Document) {
        $url_request = $this->getToken() . "sendDocument";
        $request = curl_init($url_request);
        $post = array(
            'chat_id'   => $chat_id ,
            'document'  => '@' . $Document
        );
        curl_setopt($request , 'CURLOPT_POST' , 1);
        curl_setopt($request , 'CURLOPT_POSTFIELDS' , $post);
        curl_setopt($request , 'CURLOPT_RETURNTRANSFER' , 1);
        curl_setopt($request , 'CURLOPT_PROXY' , $this->getProxy());
        curl_exec_async($request , function () {});
        curl_close ($request);
    }
    
    /**
     * Отправить Массив строку
     */
    public function sendArrayText_id ($chat_id , $ArrayText) {
        $text = null;
        foreach ($ArrayText as $value) {
            $text .= urldecode($value) . urlencode("\r\n");
        }
        $url_request = $this->getToken() . "sendMessage?chat_id=$chat_id&text=$text";
        $request = curl_init($url_request);
        curl_setopt($request , 'CURLOPT_PROXY' , $this->getProxy());
        curl_exec_async($request , function () {});
        curl_close ($request);
    }
    
    /**
     * Отправить текст как TextМемо 
     */
    public function sendEachText_id ($chat_id , $textMemo , $count) {
        $ArrayText = str_split($textMemo , $count);
        $text = null;
        foreach ($ArrayText as $value) {
            $text .= urldecode($value) . urlencode("\r\n");
        }
        $url_request = $this->getToken() . "sendMessage?chat_id=$chat_id&text=$text";
        $request = curl_init($url_request);
        curl_setopt($request , 'CURLOPT_PROXY' , $this->getProxy());
        curl_exec_async($request , function () {});
        curl_close ($request);
    }
}