<?php
namespace app\modules;

use std, gui, framework, app;

class update extends AbstractModule {
    protected $ver;
    
    /**
     * @event jdownloader.complete 
     */
    function doJdownloaderComplete(ScriptEvent $e = null) {    
        $this->setver($this->ver);
    }

    /**
     * @event zipFile.unpackAll 
     */
    function doZipFileUnpackAll(ScriptEvent $e = null) {    
        fs::delete('dist.zip');
        execute('java -jar gb.jar');
    }
    
    function updatecheck() {
        $MainForm = app()->getForm(MainForm);
        $get = file_get_contents('http://unityassetsfree.besaba.com/updategb/version.txt');
        $ver = $this->version->get('version' , 'section');
        if($ver != $get) {
            $MainForm->toast('Будет сейчас установлена последняя версия!');
            $MainModule = new contextMenuModule();
            $MainModule->Menu(true);
            $this->ver = $get;
            $this->jdownloader->start();
        }
        else {
            $MainForm->toast('У вас последняя версия :)');
        }
    }
    
    function setver($ver) {
        $this->version->set('version' , $ver ,'section');
        $this->zipFile->unpack('.');
    }
}
