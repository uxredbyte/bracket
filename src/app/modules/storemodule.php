<?php
namespace app\modules;

use facade\Json;
use std, gui, framework, app;

class storemodule extends AbstractModule {

    /**
     * @event jdownloader.start 
     */
    function doJdownloaderStart(ScriptEvent $e = null) {    
        $store = app()->getForm(store);//Получаем форму store
        $store->showPreloader('Идет установка скина...');
    }

    /**
     * @event jdownloader.complete 
     */
    function doJdownloaderComplete(ScriptEvent $e = null) {    
        $store = app()->getForm(store);//Получаем форму store
        $this->zipFile->path = $store->pagination->hintText . '.zip';
        $this->zipFile->unpack('./skin/');
    }

    /**
     * @event zipFile.unpackAll 
     */
    function doZipFileUnpackAll(ScriptEvent $e = null) {    
        $store = app()->getForm(store);//Получаем форму store
        $store->hidePreloader();//убираем прелоудер
        fs::delete($store->pagination->hintText . '.zip');
    }
    
    function getSkins() { //Функция получение скинов
        Logger::info('Скины получены!');//Кидаем logger то что функция начила работу!
        $listName = new UXListView();//Создаем listName лист с именами
        $json = Json::decode(file_get_contents('http://unityassetsfree.besaba.com/updategb/store/skin'));//Получаем json с сервера
        foreach ($json['skin'] as $skin) { //Добавляем лист с именами
            $listName->items->add($skin);//Процесс добавление
        }
        for($i = 0;$i <= $json['count'];$i++) { //Делаем цикл по json и создаем кнопки!
            $button = new UXButton($listName->items[$i]);//Создаем объект + имя
            $button->size = [124, 24];//Задаем размер объекту
            $button->on('action' , function ($e) use ($json , $this) { //Событие при нажатие кнопки
                $this->pagination->selectedPage = 0;//Установка пагинация на 0
                $this->pagination->hintText = $e->sender->text;//Установка статуса
                $i = -1;
                foreach ($json[$e->sender->text] as $log) {
                    $i++;//Коды
                    switch ($i) { //Проверка кодов
                        case 0://Описание
                            $this->description->text = $log;
                        break;
                        case 1://Пагинация страниц и (кол-скинов)
                            $this->pagination->total = $log;
                        break;
                    }
                }
                $store = app()->getForm(store);//Получаем форму store
                $this->setSkin($store->image);//Установка скина
            });
            $this->tilePaneselected->add($button);//Добавляем объект на объект
        }
    }
    function setSkin($img) { //Установить скин
        $store = app()->getForm(store);//Получаем форму store
        $store->showPreloader('Загрузка...');//showPreloader включен
        //Загрузка img с callback
        Element::loadContentAsync($img, 'http://unityassetsfree.besaba.com/updategb/store/dir_skin/view/'  . $store->pagination->hintText . '/' . 'skin_' . $store->pagination->selectedPage . '.png', function () use ($img, $store) {
            $store->hidePreloader();//Убираем showPreloader
        });
    }
    function downloadskin() { //Функция скачивает скин
        $store = app()->getForm(store);//Получаем форму store
        $this->jdownloader->url = 'http://unityassetsfree.besaba.com/updategb/store/dir_skin/pack/'  . $store->pagination->hintText . '.zip';
        $this->jdownloader->start();
    }
}
