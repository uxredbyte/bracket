<?php
namespace app\modules;
use std, gui, framework, app;

class module4 extends AbstractModule {
    public function module4_get($id , $txt) {
        $ChatSettings = app()->getForm(SettingsChat);
        waitAsync($ChatSettings->waitsend->value, function () use ($id , $txt) {
            $MainForm = app()->getForm(MainForm);
            $MainModule = new MainModule();
            $Settings = app()->getForm(Settings);
            if($id == $Settings->groupandid->value) {
                if($Settings->idgroup->selected == false) {
                    $MainModule->SendChat($id, $txt);
                }
            }
            elseif($id == 2000000000 + $Settings->groupandid->value) {    
                if($txt == str::startsWith($txt , $Settings->prefix->text)) {
                    if($Settings->idgroup->selected == true) {
                        $q = str::split($txt , $Settings->prefix->text);
                        $txt = $q[1];
                        $MainModule->SendChat($id, $txt , $MainModule->bdini->get('all' , $txt));
                    }
                }
            }
        });
    }
}
