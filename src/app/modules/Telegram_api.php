<?php
namespace app\modules;

use app\classes\jTelegramApi;
use php\jsoup\Jsoup;
use facade\Json;
use std, gui, framework, app;

class Telegram_api extends AbstractModule {
    
    /**
     * @event timer.action 
     */
    function doTimerAction(ScriptEvent $e = null) {
        $form = app()->getForm(Settings);
        $chat_id = $form->chatid->text;
        $jTelegramApi = new jTelegramApi();
        $Core = new MainModule();
        $e->sender->interval = $form->intervalsendmessage->value;
        $message = $jTelegramApi->LongPoll(0);
        $Core = new MainModule();
        $Core->SendChat(0 , $message , true);
    }

    public function eachLine ($array , $int) {
            $text = str_split($array , $int);
            $f = null;
        foreach ($text as $key) {
            $f .= urlencode($key . "\r\n");
        }
        return $f;
    }
    
    public function ArrayeachLine ($Array) {
            $f = null;
        foreach ($Array as $key) {
            $f .= urlencode($key . "\r\n");
        }
        return $f;
    }
    
    public function getListModules () {
        $modules = fs::scan('modules/Telegram_api/', ['extensions' => ['php'], 'excludeDirs' => true]);
        return $modules;
    }
    
    /**
     * Возвращаем прокси
     * @return string 
     */
    public function getProxy () {
        return app()->getForm(Settings)->proxyTelegram->text;
    }
    
    /**
     * Возвращаем токен
     * @return string 
     */
    public function getToken () {
        return app()->getForm(Settings)->token->text;
    }
    
    /**
     * Возвращаем chatid
     * @return string 
     */
    public function getChatid () {
        return app()->getForm(Settings)->chatid->text;
    }
    
    /**
     * Возвращаем интервал
     * @return int 
     */
    public function getInterval () {
        return app()->getForm(Settings)->intervalsendmessage->value;
    }
}