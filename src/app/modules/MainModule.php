<?php
namespace app\modules;

use php\lang\Thread;
use std, gui, framework, app;
use app\modules\vkModule as VK;

class MainModule extends AbstractModule {

    /**
     * @event dirChooser.action 
     */
    function doDirChooserAction(ScriptEvent $e = null) {    
        $form = app()->getForm(Settings);
        $files = fs::scan($e->sender->file , ['extensions' => ['jpg', 'png', 'gif', 'jpeg'], 'excludeDirs' => true]);
        foreach ($files as $val) {
            $img = str::replace($val , $e->sender->file . '/' , "");
            $form->list->items->add($img);
            $s = new MainModule ();
        }
    }

    /**
     * @event fileChooserimg.action 
     */
    function doFileChooserimgAction(ScriptEvent $e = null) { 
        $form = app()->getForm(Settings);
        if ($form->checkbox_img->selected && !$form->magicmodules->selected) {
            Stream::of($e->sender->file)->eachLine(function($val) use ($form) {
                $form->list->items->add(str::replace(urldecode(trim($val)) , " " , ""));
            });
        } else {
            if ($form->magicmodules->selected) {
                $form->toast('Открыть в режиме модуля нельзя!');
            } else { 
                Stream::of($e->sender->file)->eachLine(function($val) use ($form) {
                    $form->list->items->add($val);
                });
            }
        }
    }
    
    /**
     * Сохранение настройки 
     */
    function Save() {
        $Settings = app()->getForm(Settings);
        $MainForm = app()->getForm(MainForm);
        $ChatSettings = app()->getForm(SettingsChat);
        //Male
        $this->ini->set('MaleName' , $Settings->Namemale->text , 'SettingsMale');
        //Female
        $this->ini->set('FemaleName' , $Settings->NameFemale->text , 'SettingsFemale');
        $this->ini->set('waitsend' , $ChatSettings->waitsend->value , 'SettingsFemale');
        $this->ini->set('clearchatauto' , $ChatSettings->clearchatauto->selected , 'SettingsFemale');
        $this->ini->set('clearchatauto_interval' , $ChatSettings->intervaltext->value , 'SettingsFemale');
        $this->ini->set('dateandtime' , $ChatSettings->datamessage->selected , 'SettingsFemale');
        //Widget
        $this->ini->set('WidgetX' , $Settings->valueX->value , 'SettingsFemale');
        $this->ini->set('WidgetY' , $Settings->valueY->value , 'SettingsFemale');
        //AlwaysOnTop
        $this->ini->set('AlwaysOnTop' , $MainForm->alwaysOnTop , 'view');
        //colorAdjustEffect
        $this->ini->set('selected' , $Settings->colorcorrection->selected , 'colorAdjustEffect');
        $this->ini->set('brightness' , $Settings->brightness->value , 'colorAdjustEffect');
        $this->ini->set('contrast' , $Settings->contrast->value , 'colorAdjustEffect');
        $this->ini->set('hue' , $Settings->hue->value , 'colorAdjustEffect');
        $this->ini->set('saturation' , $Settings->saturation->value , 'colorAdjustEffect');
        //shadowEffect
        $this->ini->set('selected' , $Settings->selectedShadow->selected , 'shadowEffect');
        $this->ini->set('color' , $Settings->colorPicker->value , 'shadowEffect');
        $this->ini->set('radius' , $Settings->radiusShadow->value , 'shadowEffect');
        $this->ini->set('Intensity' , $Settings->Intensity->value , 'shadowEffect');
        $this->ini->set('offsetX' , $Settings->transetX->value , 'shadowEffect');
        $this->ini->set('offsetY' , $Settings->transetY->value , 'shadowEffect');
        //skin
        $this->ini->set('path' , 'skin/' . $Settings->Category_skin->selected . '/' . $Settings->skin->selected , 'SettingsFemale');
        $this->ini->set('size' , $MainForm->size , 'SettingsFemale');
        //vk
        $this->ini->set('longpoll' , $Settings->longpoll->selected , 'vk');
        $this->ini->set('selectedGroupandUsert' , $Settings->groupandid->value , 'vk');
        $this->ini->set('id' , $Settings->idgroup->selected , 'vk');
        $this->ini->set('prefix' , $Settings->prefix->text , 'vk');
    }
    /**
     * Сохранение бд
     */
    function savebd () {
        $this->bdini->set('key' , $Settings->list->itemsText , $Settings->bd->selected);
        $this->bdini->set('all' , $Settings->allprice->selected , $Settings->bd->selected);
    }
    
    /**
     * Загрузка настройки
     */
    function Load() {
        $Settings = app()->getForm(Settings);//Получаем форму Settings
        $MainForm = app()->getForm(MainForm);//Получаем форму MainForm
        //------------------------------------------
            $MainForm->title = $this->ini->get('FemaleName' , 'SettingsFemale');//Название формы равняется имени бота
        //------------------------------------------
        $ChatSettings = app()->getForm(SettingsChat);//Получаем форму SettingsChat
        //MaleName
        $Settings->Namemale->text = $this->ini->get('MaleName' , 'SettingsMale');
        //FemaleName
        $Settings->NameFemale->text = $this->ini->get('FemaleName' , 'SettingsFemale');
        $ChatSettings->waitsend->value = $this->ini->get('waitsend' , 'SettingsFemale');
        $ChatSettings->clearchatauto->selected = $this->ini->get('clearchatauto' , 'SettingsFemale');
        $ChatSettings->intervaltext->value = $this->ini->get('clearchatauto_interval' , 'SettingsFemale');
        $ChatSettings->datamessage->selected = $this->ini->get('dateandtime' , 'SettingsFemale');
        //Widget
        $Settings->valueX->value = $this->ini->get('WidgetX' , 'SettingsFemale');
        $Settings->valueY->value = $this->ini->get('WidgetY' , 'SettingsFemale');
        //widgetLoad
        $screen = UXScreen::getPrimary();
        $ScreenWidth = $screen->bounds['width'] / 2;//ширина
        $ScreenHeight = $screen->bounds['height'] / 2;//высота
        $MainForm->x = $ScreenWidth + $this->ini->get('WidgetX' , 'SettingsFemale');
        $MainForm->y = $ScreenHeight + $this->ini->get('WidgetY' , 'SettingsFemale');
        //leafeffect
        //colorAdjustEffect
        $Settings->colorcorrection->selected = $this->ini->get('selected' , 'colorAdjustEffect');
        $Settings->brightness->value = $this->ini->get('brightness' , 'colorAdjustEffect');
        $Settings->contrast->value = $this->ini->get('contrast' , 'colorAdjustEffect');
        $Settings->hue->value = $this->ini->get('hue' , 'colorAdjustEffect');
        $Settings->saturation->value = $this->ini->get('saturation' , 'colorAdjustEffect');
        //shadowEffect
        $Settings->selectedShadow->selected = $this->ini->get('selected' , 'shadowEffect');
        $Settings->colorPicker->value = UXColor::of($this->ini->get('color' , 'shadowEffect'));
        $Settings->radiusShadow->value = $this->ini->get('radius' , 'shadowEffect');
        $Settings->Intensity->value = $this->ini->get('Intensity' , 'shadowEffect');
        $Settings->transetX->value = $this->ini->get('offsetX' , 'shadowEffect');
        $Settings->transetY->value = $this->ini->get('offsetY' , 'shadowEffect');
        //AlwaysOnTop
        $MainForm->alwaysOnTop = $this->ini->get('AlwaysOnTop' , 'view');
        //bd
        $Settings->bd->itemsText = $this->bdini->get('key' , 'section');
        $Settings->bd->selectedIndex = 0;
        $Settings->selectedbd();
        //$Settings->list->itemsText = $this->bdini->get('key' , $Settings->bd->selected);
        //Skin
        $pathskin = str::split($this->ini->get('path' , 'SettingsFemale') , '/');
        $Settings->Category_skin->selected = $pathskin[1];
        $Settings->skin->selected = $pathskin[2];
        $MainForm->image->image = new UXImage($this->ini->get('path' , 'SettingsFemale'));
        $MainForm->image->size = $this->ini->get('size' , 'SettingsFemale');
        $Settings->sizeimage->value = $MainForm->image->size[0];
        //vk
        $Settings->groupandid->value = $this->ini->get('selectedGroupandUsert' , 'vk');
        $Settings->idgroup->selected = $this->ini->get('id' , 'vk');
        $Settings->prefix->text = $this->ini->get('prefix' , 'vk');
        $this->vklogin();//вк загрузка
        //------------------------------------MODULES SYSTEM
            $this->getmodules();//загрузка модулей
        //------------------------------------MODULES SYSTEM
        //Проверка обновление
        $update = new update();
        $update->updatecheck();//Проверка обновление
    }
    function getmodules() {
        $modules = fs::scan('modules/', ['extensions' => ['php'], 'excludeDirs' => true]);
        $Settings = app()->getForm(Settings);
        foreach ($modules as $module) {
            $Settings->selectedmodule->items->add($module);
        }
        $Settings->selectedmodule->selectedIndex = 0;
    }
    function getmoduleslist() {
        $Settings = app()->getForm(moduleslist);
        $Settings->moduleiteam->items->clear();
        $modules = fs::scan('modules/', ['extensions' => ['php'], 'excludeDirs' => true]);
        foreach ($modules as $iteam) {
            $Settings->moduleiteam->items->add($iteam);
        }
        $Settings->moduleiteam->selectedIndex = 0;
    }
    function LoadLeaf($e) {
        //colorAdjustEffect
        if($this->ini->get('selected' , 'colorAdjustEffect') == false) {
            $e->colorAdjustEffect->disable();
            $e->colorAdjustEffect->brightness = $this->ini->get('brightness' , 'colorAdjustEffect');
            $e->colorAdjustEffect->contrast = $this->ini->get('contrast' , 'colorAdjustEffect');
            $e->colorAdjustEffect->hue = $this->ini->get('hue' , 'colorAdjustEffect');
            $e->colorAdjustEffect->saturation = $this->ini->get('saturation' , 'colorAdjustEffect');
        }
        elseif($this->ini->get('selected' , 'colorAdjustEffect') == true) {
           $e->colorAdjustEffect->enable();
           $e->colorAdjustEffect->brightness = $this->ini->get('brightness' , 'colorAdjustEffect');
           $e->colorAdjustEffect->contrast = $this->ini->get('contrast' , 'colorAdjustEffect');
           $e->colorAdjustEffect->hue = $this->ini->get('hue' , 'colorAdjustEffect');
           $e->colorAdjustEffect->saturation = $this->ini->get('saturation' , 'colorAdjustEffect');
        }
        //shadowEffect
        if($this->ini->get('selected' , 'shadowEffect') == false) {
            $e->dropShadowEffect->disable();
            $e->dropShadowEffect->color = $this->ini->get('color' , 'shadowEffect');
            $e->dropShadowEffect->radius = $this->ini->get('radius' , 'shadowEffect');
            $e->dropShadowEffect->spread = $this->ini->get('Intensity' , 'shadowEffect');
            $e->dropShadowEffect->offsetX = $this->ini->get('offsetX' , 'shadowEffect');
            $e->dropShadowEffect->offsetY = $this->ini->get('offsetY' , 'shadowEffect');
        }
        elseif($this->ini->get('selected' , 'shadowEffect') == true) {
           $e->dropShadowEffect->enable();
           $e->dropShadowEffect->color = $this->ini->get('color' , 'shadowEffect');
           $e->dropShadowEffect->radius = $this->ini->get('radius' , 'shadowEffect');
           $e->dropShadowEffect->spread = $this->ini->get('Intensity' , 'shadowEffect');
           $e->dropShadowEffect->offsetX = $this->ini->get('offsetX' , 'shadowEffect');
           $e->dropShadowEffect->offsetY = $this->ini->get('offsetY' , 'shadowEffect');
        }
    }
    
    public function SendChat($id , $txt , $all) {
        $chat = app()->getForm(chat);
        $Settings = app()->getForm(Settings);
        $settingschat = app()->getForm(SettingsChat);
        $Name = $this->ini->get('MaleName' , 'SettingsMale');
        $FemaleName = $this->ini->get('FemaleName' , 'SettingsFemale');
        if ($settingschat->datamessage->selected) {
            $date = Time::now();
            $chat->textArea->appendText("[$date][$Name]->$txt\n");
        } else {
            $chat->textArea->appendText("[$Name]->$txt\n");
        }
        waitAsync($this->ini->get('waitsend' , 'SettingsFemale') , function () use ($Settings , $chat , $FemaleName , $txt , $id , $all) {
            if (str::startsWith($txt , '/')) {
                $val = explode('@' , $txt); // 0
                $str = explode(' ' , $val[0]); // 2
                if (count($str) != 0) {
                    $GLOBALS['last_text'] = $str[count($str) - 1];
                    array_pop($str);
                }
                $coco = null;
                foreach ($str as $sd) {
                    $coco .= $sd . ' '; 
                }
                if($str[count($str) - 1] != null) {
                    $Settings->bd->selected = trim($coco);
                    if (!$Settings->list->items->isEmpty()) {
                        $GLOBALS['txt'] = $txt;
                        $module = new Module($Settings->list->items[0]);
                        $module->cleanData();
                        $module->call();
                    }
                } 
                else {
                    $Settings->bd->selected = $val[0];
                    if (!$Settings->magicmodules->selected) {
                        $GLOBALS['txt'] = $txt;
                        $module = new Module($Settings->list->items[0]);
                        $f = $Settings->list->items[rand(0 , $Settings->list->items->count - 1)];
                        $settingschat = app()->getForm(SettingsChat);
                        $this->clearfirstiteam ($FemaleName , $f);
                        $TelegramAPI = new Telegram_api();
                        $TelegramAPI->setToken($Settings->token->text);
                        $TelegramAPI->sendMessage_id($Settings->chatid->text , $f);
                    } else {
                        if (!$Settings->list->items->isEmpty()) {
                            $GLOBALS['txt'] = $txt;
                            $module = new Module($Settings->list->items[0]);
                            $module->cleanData();
                            $module->call();
                        }
                    }
                }
            }
        /*
            if ($Settings->Asynx_token->selected and str::length($txt) == str::length("(ascii_gif_random=z") or str::length($txt) == str::length("(ascii_gif_random=zz")) { //Магические команды Telegram Api
                $core = new MainModule();
                $url = 'http://text-image.ru/news/animacija_ascii_art/4-0-68';
                $this->jsoup->parseAsync($url , function () use ($txt , $Settings) {
                    $data = $this->jsoup->find('.swchItem1')->select('a');
                    $val[];
                    foreach ($data as $item) {
                        if ($item->attr('href') == '/news/animacija_ascii_art/3-0-68'){
                            array_push($val , "/news/animacija_ascii_art/3-0-68");  
                            array_push($val , "/news/animacija_ascii_art/4-0-68");    
                        } else {
                            array_push($val , $item->attr('href'));    
                        }
                    }
                    $val_kek = str::split($txt , '=');
                    if ($val_kek[1] < count($val) and $val_kek[1] > 0) {
                        $url = 'http://text-image.ru' . $val[$val_kek[1]];
                        $this->jsoup->parseAsync($url , function () use ($Settings) {
                            $data = $this->jsoup->findFirst('.archiveEntries')->select('img');
                            $val[];
                            foreach ($data as $item) {
                                array_push($val , $item->attr('src'));
                            }
                            $TelegramAPI = new Telegram_api();
                            $TelegramAPI->setToken($Settings->token->text);
                            $TelegramAPI->sendMessage_id($Settings->chatid->text , 'http://text-image.ru' . $val[rand(1 , count($val) - 1)]);
                            return ;
                        });
                    }
                });
            } 
            } else if ($Settings->Asynx_token->selected and $txt == "(neon_text") {
                $s = "test";
                app()->getForm(viewneon)->show();
                app()->getForm(viewneon)->browser->engine->loadContent('<div><html lang="en"><body><iframe src="http://ntmaker.gfto.ru/newneontext/?image_height=200&image_width=600&image_font_shadow_width=30&image_font_size=80&image_background_color=FFFFFF&image_text_color=FF91A9&image_font_shadow_color=F7406B&image_url=&image_text=' . $s . '&image_font_family=Nickainley&" frameborder="no" scrolling="no" width="600" height="200"></iframe></body></html></>');
            }
            */
            else {
                if($this->bdini->get('key' , $txt) != null) {
                   if($Settings->longpoll->selected) {
                       if($all == true) {
                           $Settings->bd->selected = $txt;
                           $f = $Settings->list->itemsText;
                           $settingschat = app()->getForm(SettingsChat);
                           $this->clearfirstiteam ($FemaleName , $f);
                           if($Settings->idgroup->selected == true) {
                               VK::Query('messages.send', ['chat_id'=> $Settings->groupandid->value, 'message'=>$f]);
                           } 
                           else {
                               VK::Query('messages.send', ['user_id'=> $id, 'message'=>$f]);
                           }
                           return;
                       }
                       else {
                           $Settings->bd->selected = $txt;
                           $f = $Settings->list->items[rand(0 , $Settings->list->items->count - 1)];
                           $settingschat = app()->getForm(SettingsChat);
                           $this->clearfirstiteam ($FemaleName , $f);
                           if($Settings->idgroup->selected == true) {
                               VK::Query('messages.send', ['chat_id'=> $Settings->groupandid->value, 'message'=>$f]);
                           }
                           else {
                               VK::Query('messages.send', ['user_id'=> $id, 'message'=>$f]);
                           }
                           return;
                       }
                   }
                   else {
                       if($all == false)  {
                           if($Settings->longpoll->selected == false && $Settings->Asynx_token->selected == false)  {
                               $Settings->bd->selected = $txt;
                               $f = $Settings->list->items[rand(0 , $Settings->list->items->count - 1)];
                               $settingschat = app()->getForm(SettingsChat);
                               $this->clearfirstiteam ($FemaleName , $f);
                               return ;
                           } else if ($Settings->Asynx_token->selected) {
                                   $Settings->bd->selected = $txt;
                                   $f = $Settings->list->items[rand(0 , $Settings->list->items->count - 1)];
                                   $settingschat = app()->getForm(SettingsChat);
                                   $this->clearfirstiteam ($FemaleName , $f);
                                   $TelegramAPI = new Telegram_api();
                                   $TelegramAPI->setToken($Settings->token->text);
                                   $TelegramAPI->sendMessage_id($Settings->chatid->text , $f);
                                   return ;
                               }
                           } else {
                           $Settings->bd->selected = $txt;
                           $f = null;
                           foreach ($Settings->list->items as $val) {
                               $str = str::length($val);
                               $strlg = str::length('                                                                                                                                            ');
                               $str = $strlg -= $str;
                               var_dump($str);
                               $str_out = str::repeat(' ' , $str);
                               $f .= $str_out . $val;
                           }
                           $settingschat = app()->getForm(SettingsChat);
                           $this->clearfirstiteam ($FemaleName , $f);
                           $TelegramAPI = new Telegram_api();
                           $TelegramAPI->setToken($Settings->token->text);
                           $TelegramAPI->sendMessage_id($Settings->chatid->text , $f);
                           return ;
                       }
                   }
                }
            }
        });
    }
    
    /**
     * Авто-удаление
     */
    function clearfirstiteam ($Name , $text) {
        $settingschat = app()->getForm(SettingsChat);
        if ($settingschat->clearchatauto->selected) {
            $chat = app()->getForm(chat);
            if ($settingschat->datamessage->selected) {
                $date = Time::now();
                $chat->textArea->appendText("[$date][$Name]->$text\n");
            } else {
                $chat->textArea->appendText("[$Name]->$text\n");
            }
            $iteam = new UXListView();
            $text = null;
            $i = null;
            foreach (str::split($chat->textArea->text , "\n") as $value) {
                $i++;
                if ($i > app()->getForm(SettingsChat)->intervaltext->value) {
                    $iteam->items->removeByIndex(0);
                    $iteam->items->add($value);
                } else {
                    $iteam->items->add($value);
                }
            }     
            foreach ($iteam->items as $val) {
                $text .= $val . "\n";
            }
            $chat->textArea->text = $text;
        } else {
            if ($settingschat->datamessage->selected) {
                $date = Time::now();
                $chat->textArea->appendText("[$date][$Name]->$text\n");
            } else {
                $chat->textArea->appendText("[$Name]->$text\n");
            }
        }
    } 
    
    /**
     * Авто-удаление в чате
     */
    function clearfirstiteamprofile () {
        $settingschat = app()->getForm(SettingsChat);
        if ($settingschat->clearchatauto->selected) {
            $chat = app()->getForm(chat);
            $iteam = new UXListView();
            $text = null;
            $i = null;
            foreach (str::split($chat->textArea->text , "\n") as $value) {
                $i++;
                if ($i > app()->getForm(SettingsChat)->intervaltext->value) {
                    $iteam->items->removeByIndex(0);
                    $iteam->items->add($value);
                } else {
                    $iteam->items->add($value);
                }
            }     
            foreach ($iteam->items as $val) {
                $text .= $val . "\n";
            }
            $chat->textArea->text = $text;
        }
    } 
    
    function vklogin() {
        $Settings = app()->getForm(Settings);
        $MainForm = app()->getForm(MainForm);
        if(VK::isAuth() == false) {
            $Settings->loginvk->graphic = new UXImageView (new UXImage('res://.data/img/Exit.png'));
            $Settings->longpoll->enabled = false;
        }
        else {
            $Settings->loginvk->graphic = new UXImageView (new UXImage('res://.data/img/action.png'));
            $Settings->longpoll->enabled = true;
            $MainForm->toast('Авторизован вк -> успешно!');
        }
    }
    /**
     * Загрузка скина
     */
    function reloadskin () {
        $form = app()->getForm(Settings);
        $files = fs::scan('skin/', ['excludeFiles' => true]);
        $form->Category_skin->items->clear();
        foreach ($files as $fs) {
        /*
            if (str::contains($fs , '/')) {
                
            } else if (str::contains($fs , "\\") {
                
            }
            */
            $path = str::split($fs , "\\");
            $form->Category_skin->items->add($path[1]);
        }
        $form->skin->items->clear();
        $f = fs::scan('skin/' . $form->Category_skin->selected . '/', ['extensions' => ['jpg', 'png' , 'jpeg'], 'excludeDirs' => true]);
        foreach ($f as $fsk) {
            $paths = str::split($fsk , "\\");
            $form->skin->items->add($paths[2]);
        }
    }
}