<?php
use app , std , framework , gui;

UXApplication::runLater(function () {
	main();
});

function main () {
	$form = new app\modules\Telegram_api;
	$api = new app\classes\jTelegramApi;
	$api->sendMessage_id($form->getChatid() , createform());
}

function createform () {
	$form = new UXForm();
	$form->size = [640,480];
	$form->show();
	return 'Форма успешно создана!';
}