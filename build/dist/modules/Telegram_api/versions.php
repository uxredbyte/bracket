<?php
use app , std , framework , gui;

UXApplication::runLater(function () {
	main();
});

function main () {
	$form = new app\modules\Telegram_api;
	$api = new app\classes\jTelegramApi;
	$api->sendArrayText_id($form->getChatid() , getVersions());
}

function getVersions () {
	$form = new app\modules\Telegram_api;
	$list = [
		"[jppm => " . constant('JPHP_VERSION') . ']' ,
		"[Установлена модулей => " . count($form->getListModules()) . ']' . '[Telegram_api]' ,
	];
	$i = 0;
	foreach ($form->getListModules() as $value) {
		$i++;
		array_push($list, "->[$i]" . "[$value]");
	}
	array_push($list, "[Установлена модулей => " . 0 . ']' . '[vk_api]');
	return $list;
}
