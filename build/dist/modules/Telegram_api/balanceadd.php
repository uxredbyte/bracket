<?php
use app , std , framework , gui;

UXApplication::runLater(function () {
	main();
});

function main () {
	$form = new app\modules\Telegram_api;
	$api = new app\classes\jTelegramApi;
	$command = $GLOBALS['txt'];
	$val = explode(' ', $command);
	$api->sendMessage_id($form->getChatid() , '[' . $GLOBALS['getname'] . ']' . "[$command]" . addValue($val[2]));
}

function addValue ($value) {
	if(str::length($value) < 15) {
		if(str::isNumber($value)) {
			$iduser = $GLOBALS['getlastmessage'];
			$val = $GLOBALS[$iduser . '_balance'] + $value;
			$GLOBALS[$iduser . '_balance'] = $val;
			return "[OK]" . " => " . $value;
		} else {
			return "[ERROR]" . " => " . 'Можно только цифры';
		}
	} else {
		return "[ERROR]" . " => " . 'Нельзя больше чем 15 символов';
	}
}