<?php
use app , std , framework , gui;

UXApplication::runLater(function () {
	main();
});

function main () {
	$form = new app\modules\Telegram_api;
	$api = new app\classes\jTelegramApi;
	$api->sendMessage_id($form->getChatid() , '[' . $GLOBALS['getname'] . ']' . '[OK] => ' . getValue());
}

function getValue () {
	$iduser = $GLOBALS['getlastmessage'];
	return $GLOBALS[$iduser . '_balance'];
}