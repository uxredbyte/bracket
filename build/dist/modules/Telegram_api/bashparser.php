<?php
use app , std , framework , gui;
use php\jsoup\Jsoup;

UXApplication::runLater(function () {
	main();
});

function main () {
	$form = new app\modules\Telegram_api;
	$api = new app\classes\jTelegramApi;
	$doc = Jsoup::connect("https://bash.im/")->get();
	$newsHeadlines = $doc->select(".quote");
	$iteam[];
	foreach ($newsHeadlines as $element) {
		array_push($iteam, $element->text());
	}
	$val = $iteam[rand(1 , count($iteam) - 1)];
	$api->sendMessage_id($form->getChatid() , $form->eachLine($val , 50));
}