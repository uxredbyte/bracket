<?php

use app , std , framework , gui;

UXApplication::runLater(function () {
	main();
});

function main () {
	$form = new app\modules\Telegram_api;
	$api = new app\classes\jTelegramApi;
	$f = null;
	$i = 0;
	foreach ($form->getListModules() as $val) {
		$i++;
		$s = explode('/', $val);
		if ($s[count($s) - 1] == "ListModules.php") {
			$f .= $str_out . $val . "<-[$i] [Получить все модули]" . '[/getlistmodules]' . urlencode("\r\n");
		} else if ($s[count($s) - 1] == 'genuuid.php') {
			$f .= $str_out . $val . "<-[$i] [рандомная генерация uuid]" . '[/genuuid]' . urlencode("\r\n");
		} else if ($s[count($s) - 1] == 'strRandom.php') {
			$f .= $str_out . $val . "<-[$i] [рандомная генерация символов]" . '[/genrandom]' . urlencode("\r\n");
		} else if ($s[count($s) - 1] == 'anime.php') {
			$f .= $str_out . $val . "<-[$i] [Аниме]" . '[/anime_random => Кидает рандомную пикчу]' . urlencode("\r\n");
		} else if ($s[count($s) - 1] == 'versions.php') {
			$f .= $str_out . $val . "<-[$i] [О версий]" . '[/versions]' . urlencode("\r\n");
		} else if ($s[count($s) - 1] == 'bashparser.php') {
			$f .= $str_out . $val . "<-[$i] [bash цитаты]" . '[/bash_random]' . urlencode("\r\n");
		} else if ($s[count($s) - 1] == 'test.php') {
			$f .= $str_out . $val . "<-[$i] [Тестовый модуль]" . '[/?????????]' . urlencode("\r\n");
		}
	}
	$api->sendMessage_id($form->getChatid() , $f);
}